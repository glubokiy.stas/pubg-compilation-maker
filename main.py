import sys
from functools import lru_cache
from math import ceil, floor
from pathlib import Path
from typing import List

import cv2
import numpy as np
from moviepy.config import get_setting
from moviepy.tools import subprocess_call
from moviepy.video.compositing.concatenate import concatenate_videoclips
from moviepy.video.io.VideoFileClip import VideoFileClip


class Video:

    def __init__(self, path):
        self.path = path

    @property
    def frames(self):
        return VideoFrames(self)

    @property
    @lru_cache()
    def fps(self):
        return VideoFrames(self).get_fps()


class VideoFrames:

    def __init__(self, video: Video):
        self.cap = cv2.VideoCapture(video.path)

    def __iter__(self):
        frame_num = 0
        while self.cap.isOpened():
            ret, frame = self.cap.read()

            if not ret:
                break

            yield frame_num, frame

            frame_num += 1

    def __del__(self):
        self.cap.release()

    def get_fps(self):
        return self.cap.get(cv2.CAP_PROP_FPS)


def get_kill_info(frame):
    return frame[750:790, 890:1030, :]


@lru_cache()
def get_sample():
    return cv2.imread((Path(__file__).parent / "sample.png").as_posix(), flags=0)


def calc_binary_similarity(first, second):
    num_pixels = np.product(first.shape)

    num_unmatched_pixels = np.sum(np.bitwise_xor(first, second) / 255)
    return 1 - num_unmatched_pixels / num_pixels


def has_frame_kill_info(frame, score_threshold=0.80) -> bool:
    kill_info_candidate = get_kill_info(frame)
    candidate_binary = cv2.threshold(kill_info_candidate[:, :, 2], 215, 255, cv2.THRESH_BINARY)[1]
    sample = get_sample()

    sample_width = sample.shape[1]
    max_offset = candidate_binary.shape[1] - sample_width

    for offset in range(30, max_offset):
        candidate_crop = candidate_binary[:, offset:offset+sample_width]

        score = calc_binary_similarity(candidate_crop, sample)
        if score > score_threshold:
            return True

    return False


def kills_differ(first_frame, second_frame):
    first, second = [
        cv2.threshold(get_kill_info(im)[:, :, 2], 215, 255, cv2.THRESH_BINARY)[1]
        for im in [first_frame, second_frame]
    ]
    return calc_binary_similarity(first, second) < 0.9


def find_unique_kill_frames(video: Video) -> List[int]:
    """Return list of frame numbers with kill info"""
    print("Looking for frames with kill info (checking every 10th frame)")
    unique_kill_frames = []
    last_found = None
    for frame_num, frame in video.frames:
        if frame_num % 10 == 0 and has_frame_kill_info(frame):
            # Consider kill frame a unique kill frame if either is true:
            # - it is the first kill
            # - previous kill was more than 8 seconds before
            # - kill info is different
            if (last_found is None
                or (frame_num - last_found[0]) / video.fps > 8
                or kills_differ(last_found[1], frame)
            ):
                unique_kill_frames.append(frame_num)
                last_found = (frame_num, frame)

                print(f"Found Kill #{len(unique_kill_frames)}")

    return unique_kill_frames


def consolidate_frames(kill_frame_nums: List[int], video: Video) -> List:
    fps = video.fps
    kill_ranges = [[f - 6 * fps, f + 0.8 * fps] for f in kill_frame_nums]

    compilation_ranges = []
    for start, end in kill_ranges:
        if not compilation_ranges:
            compilation_ranges.append([start, end])
            continue

        # If next range starts before the end of the previous one,
        # extend the previous range
        if start < compilation_ranges[-1][1]:
            compilation_ranges[-1][1] = end
            continue

        compilation_ranges.append([start, end])

    return compilation_ranges


def write_video(video, output_video_path, ranges_for_compilation):
    short_clip_names = []
    for start_frame, end_time in ranges_for_compilation:
        start_time = floor(start_frame / video.fps)
        end_time = ceil(end_time / video.fps)

        name = f"output_{start_time}_{end_time}.mp4"
        short_clip_names.append(name)

        cmd = [
            get_setting("FFMPEG_BINARY"),
            "-y",
            "-ss", "%0.2f" % start_time,
            "-i", video.path,
            "-t", "%0.2f" % (end_time - start_time),
            "-c", "copy",
            "-avoid_negative_ts", "1",
            name,
        ]

        subprocess_call(cmd)

    clips = [VideoFileClip(name) for name in short_clip_names]
    video = concatenate_videoclips(clips, method="chain")
    video.write_videofile(
        output_video_path,
        fps=video.fps,
        temp_audiofile='temp-audio.m4a',
        remove_temp=True,
        codec="libx264",
        audio_codec="aac",
    )

    for name in short_clip_names:
        Path(name).unlink()


def compile_pubg_kills(input_video_path: str, output_video_path: str):
    video = Video(input_video_path)
    kill_frame_nums = find_unique_kill_frames(video)
    ranges_for_compilation = consolidate_frames(kill_frame_nums, video)
    write_video(video, output_video_path, ranges_for_compilation)


def main():
    video = sys.argv[1]
    compile_pubg_kills(video, sys.argv[2])


if __name__ == '__main__':
    main()
